#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_read_2(self):
        s = "100000 1000000\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  100000)
        self.assertEqual(j, 1000000)

    def test_read_3(self):
        s = "1569 1704\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 1569)
        self.assertEqual(j, 1704)

    def test_read_4(self):
        s = "25689 137894\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  25689)
        self.assertEqual(j, 137894)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)
        
    def test_eval_5(self):
        v = collatz_eval(16, 16)
        self.assertEqual(v, 5)

    def test_eval_6(self):
        v = collatz_eval(1000000, 1000000)
        self.assertEqual(v, 153)

    def test_eval_7(self):
        v = collatz_eval(10000, 10200)
        self.assertEqual(v, 224)

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 1000000, 1000000, 153)
        self.assertEqual(w.getvalue(), "1000000 1000000 153\n")

    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 1956, 2412, 183)
        self.assertEqual(w.getvalue(), "1956 2412 183\n")

    def test_print_4(self):
        w = StringIO()
        collatz_print(w, 10000, 10200, 224)
        self.assertEqual(w.getvalue(), "10000 10200 224\n")
    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve_2(self):
        r = StringIO("16 16\n1000000 1000000\n10000 10200\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(w.getvalue(), "16 16 5\n1000000 1000000 153\n10000 10200 224\n")

    def test_solve_3(self):
        r = StringIO("11568 1249\n87 7\n10000 8956\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(w.getvalue(), "11568 1249 268\n87 7 116\n10000 8956 260\n")

    def test_solve_4(self):
        r = StringIO("11000 11500\n17852 18000\n234567 345678\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(w.getvalue(), "11000 11500 237\n17852 18000 248\n234567 345678 407\n")

#
# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
% coverage-3.5 run --branch TestCollatz.py >  TestCollatz.out 2>&1


% cat TestCollatz.out
...................
----------------------------------------------------------------------
Ran 19 tests in 6.640s

OK

% coverage-3.5 report -m                   >> TestCollatz.out



% cat TestCollatz.out
...................
----------------------------------------------------------------------
Ran 19 tests in 17.124s

OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          56      0     26      0   100%
TestCollatz.py      83      0      0      0   100%
------------------------------------------------------------
TOTAL              139      0     26      0   100%
"""
